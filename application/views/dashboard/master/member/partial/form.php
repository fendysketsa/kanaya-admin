<div class="row">
    <?php if (!empty($dataGet)) {?>
    <input form="formProduk" value="<?php echo $dataGet->id; ?>" name="id" type="hidden">
    <?php }?>

    <div class="col-lg-12">
        <div class="form-group input-field">
            <label class="active">Gambar</label>
            <div class="input-image-produk" style="padding-top:.5rem;"></div>
        </div>
    </div>

    <div class="col-lg-4">
        <div class="form-group">
            <label for="kode">Kode <em class="text-danger">*</em></label>
            <input id="kode" form="formProduk" value="<?php echo !empty($dataGet) ? $dataGet->kode : ''; ?>" name="kode"
                type="text" class="form-control required" placeholder="Kode...">
        </div>
    </div>
    <div class="col-lg-8">
        <div class="form-group">
            <label for="nama">Nama <em class="text-danger">*</em></label>
            <input form="formProduk" value="<?php echo !empty($dataGet) ? $dataGet->nama : ''; ?>" name="nama"
                type="text" id="nama" class="form-control required" placeholder="Nama...">
        </div>
    </div>
</div>

<div class="row">

    <div class="col-lg-4">
        <div class="form-group">
            <label for="hargahpp">Harga HPP <em class="text-danger">*</em></label>
            <input id="hargahpp" form="formProduk" value="<?php echo !empty($dataGet) ? $dataGet->harga_hpp : ''; ?>"
                name="harga_hpp" type="text" class="form-control required" placeholder="Harga HPP...">
        </div>
    </div>
    <div class="col-lg-5">
        <div class="form-group">
            <label for="hargajual">Harga Jual <em class="text-danger">*</em></label>
            <input form="formProduk" value="<?php echo !empty($dataGet) ? $dataGet->harga_jual : ''; ?>"
                name="harga_jual" type="text" id="hargajual" class="form-control required" placeholder="Harga jual...">
        </div>
    </div>
    <div class="col-lg-3">
        <div class="form-group">
            <label for="stok">Stok</label>
            <input id="stok" form="formProduk" value="<?php echo !empty($dataGet) ? $dataGet->stok : ''; ?>" name="stok"
                type="text" class="form-control" placeholder="Stok...">
        </div>
    </div>
</div>

<div class="form-group">
    <label for="kode">Deskripsi</label>
    <textarea form="formProduk" name="keterangan" class="form-control" cols="3" rows="2"
        placeholder="Keterangan..."><?php echo !empty($dataGet) ? $dataGet->keterangan : ''; ?></textarea>
</div>

<div class="row">

    <div class="col-lg-3">
        <div class="form-group">
            <label for="hargahpp">Posting</label>
            <select id="postorpre" class="form-control">
                <option value="1" selected>Post</option>
                <option value="2">Pre-Order</option>
            </select>
        </div>
    </div>
    <div class="col-lg-9">
        <div class="form-group">
            <label for="preorder">Pre Order <em class="text-danger">*</em></label>
            <input form="formProduk" value="<?php echo !empty($dataGet) ? $dataGet->pre_order : ''; ?>" name="pre_order"
                type="text" id="preorder" class="form-control required" placeholder="Pre Order...">
        </div>
    </div>

</div>