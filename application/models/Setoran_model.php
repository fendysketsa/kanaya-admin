<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setoran_model extends CI_Model
{

    protected $table = 'log_setoran';

    public function save($data)
    {
        $this->db->set($data);
        $this->db->insert($this->table);
        return $this->db->insert_id();
    }

    public function getId($id)
    {
        $this->db->where($this->table.".id", $id);
       //  $this->db->join('transaksi',$this->table.'.transaksi_id = transaksi.id');
        $this->db->join('pegawai',$this->table.'.marketing_id = pegawai.id');
       // $this->db->join('member','transaksi.member_id = member.id');
        return $this->db->get($this->table)->row();
    }

    public function update($id, $data)
    {
        $this->db->where("id", $id);
        return $this->db->update($this->table, $data);
    }

    public function deleteId($id)
    {
        $this->db->where("id", $id);
        return $this->db->delete($this->table);
    }


    public function allData()
    {
        // $this->db->join('transaksi',$this->table.'.transaksi_id = transaksi.id');
        // $this->db->join('pegawai','transaksi.marketing_id = pegawai.id');
        // $this->db->join('member','transaksi.member_id = member.id');
         $this->db->join('pegawai',$this->table.'.marketing_id = pegawai.id');
        return $this->db->count_all($this->table);
    }

    public function countDataFilter($search = null)
    {
        if (!empty($search)) {
            $this->db->like('pegawai.nama', $search);
         }
        // $this->db->join('transaksi',$this->table.'.transaksi_id = transaksi.id');
        // $this->db->join('pegawai','transaksi.marketing_id = pegawai.id');
        // $this->db->join('member','transaksi.member_id = member.id');
        $this->db->join('pegawai',$this->table.'.marketing_id = pegawai.id');
        return $this->db->get($this->table)->num_rows();
    }

    public function dataFilter($search = null)
    {
         if (!empty($search['search'])) {
            $this->db->like('transaksi.no_transaksi', $search['search']);
        }
        $this->db->select('pegawai.nama as pegawai, log_setoran.*');
        $this->db->order_by($this->table.'.'.$search['order_field'], $this->table.'.'.$search['order_ascdesc']);
        $this->db->limit($search['limit'], $search['offset']);
        // $this->db->join('transaksi',$this->table.'.transaksi_id = transaksi.id');
        // $this->db->join('pegawai','transaksi.marketing_id = pegawai.id');
        // $this->db->join('member','transaksi.member_id = member.id');
        $this->db->join('pegawai',$this->table.'.marketing_id = pegawai.id');
        // $this->db->where('role.nama','marketing');
        return $this->db->get($this->table)->result_array();
    }

    public function json($search = null)
    {
        $sql_total = $this->allData();
        $sql_data = $this->dataFilter($search);
        $sql_filter = $this->countDataFilter($search['search']);

        $callback = array(
            'draw' => $search['draw'],
            'recordsTotal' => $sql_total,
            'recordsFiltered' => $sql_filter,
            'data' => $sql_data,
        );

        header('Content-Type: application/json');
        echo json_encode($callback);

    }

    public function getCountSetoran()
    {
        $this->db->select($this->table.'.*, pegawai.nama as pegawai');
        $this->db->join('pegawai',$this->table.'.marketing_id = pegawai.id');
        $this->db->where('status', 0);
        return $this->db->get($this->table)->result_array();
    }

  
}
